/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginEditor.h"
#include "PluginProcessor.h"


// Util functions.
inline juce::String addKiloPrefix(float value)
{
    juce::String str;
    bool addK = false;
    
    if (value > 999.99f)
    {
        addK = true;
        value /= 1000.f;
    }
    
    str << value;
    if (addK)
    {
        str << "k";
    }
    str << "Hz";
    
    return str;
}

//==================================================================================//
void MyLookAndFeel::drawRotarySlider(juce::Graphics& g, int x, int y, int width, int height, float sliderPosProportional, float rotaryStartAngle, float rotaryEndAngle, juce::Slider &slider)
{
    using namespace juce;
    
    Path p;
    Rectangle<float> r;
    
    auto bounds = Rectangle<float>(x, y, width, height);
    
    auto enabled = slider.isEnabled();
    
    g.setColour(enabled ? Colour(28u, 93u, 153u) : Colours::darkgrey);
    g.fillEllipse(bounds);
    
    g.setColour(enabled ? Colour(34u, 34u, 34u) : Colours::grey);
    g.drawEllipse(bounds, 1.f);
    
    if (auto* rswl = dynamic_cast<RotarySliderWithLabels*>(&slider))
    {
        auto center = bounds.getCentre();
        
        r.setLeft(center.getX() - 2);
        r.setRight(center.getX() + 2);
        r.setTop(bounds.getY());
        r.setBottom(center.getY() - rswl->getTextHeight() * 1.5);
        
        p.addRoundedRectangle(r, 2.f);
        
        jassert(rotaryStartAngle < rotaryEndAngle);
        
        auto sliderAng = jmap(sliderPosProportional, 0.f, 1.f, rotaryStartAngle, rotaryEndAngle);
        
        p.applyTransform(AffineTransform().rotated(sliderAng, center.getX(), center.getY()));
        
        g.fillPath(p);
        
        g.setFont(rswl->getTextHeight());
        auto text = rswl->getDisplayString();
        auto stringWidth = g.getCurrentFont().getStringWidth(text);
        
        r.setSize(stringWidth + 4, rswl->getTextHeight() + 2);
        r.setCentre(center);
        
        g.setColour(Colours::white);
        g.drawFittedText(text, r.toNearestInt(), juce::Justification::centred, 1);
    }
}

void MyLookAndFeel::drawToggleButton(juce::Graphics &g, juce::ToggleButton &toggleButton, bool shouldDrawButtonAsHighlighted, bool shouldDrawButtonDown)
{
    using namespace juce;
    
    // Much nicer way than what he implemented
    if (auto* pb = dynamic_cast<CustomToggleButton*>(&toggleButton))
    {
        pb->drawToggleButton(g, *pb, shouldDrawButtonAsHighlighted, shouldDrawButtonDown);
    }
}

void PowerButton::drawToggleButton(juce::Graphics &g, CustomToggleButton &toggleButton, bool shouldDrawButtonAsHighlighted, bool shouldDrawButtonDown)
{
    using namespace juce;
    
    Path powerButton;
    
    auto bounds = toggleButton.getLocalBounds();
    
    auto size = jmin(bounds.getWidth(), bounds.getHeight()) - 6;
    auto r = bounds.withSizeKeepingCentre(size, size).toFloat();
    
    float ang = 30.f; //30.f;
    
    size -= 6;
    
    powerButton.addCentredArc(r.getCentreX(),
                              r.getCentreY(),
                              size * 0.5,
                              size * 0.5,
                              0.f,
                              degreesToRadians(ang),
                              degreesToRadians(360.f - ang),
                              true);
    
    powerButton.startNewSubPath(r.getCentreX(), r.getY());
    powerButton.lineTo(r.getCentre());
    
    PathStrokeType pst(2.f, PathStrokeType::JointStyle::curved);
    
    auto color = toggleButton.getToggleState() ? Colours::dimgrey : Colour(0u, 172u, 1u);
    
    g.setColour(color);
    g.strokePath(powerButton, pst);
    g.drawEllipse(r, 2);
}

bool PowerButton::hitTest(int x, int y)
{
    bool hit = false;
    
    auto bounds = getLocalBounds();
    auto size = juce::jmin(bounds.getWidth(), bounds.getHeight()) - 6;
    auto r = bounds.withSizeKeepingCentre(size, size).toFloat();
    
    if (x >= r.getX() && x <= r.getX() + size)
    {
        hit = true;
    }
    
    return hit;
}

void AnalyzerButton::drawToggleButton(juce::Graphics &g,CustomToggleButton &toggleButton,bool shouldDrawButtonAsHighlighted, bool shouldDrawButtonDown)
{
    using namespace juce;
    
    auto* button = dynamic_cast<AnalyzerButton*>(&toggleButton);
    
    auto color = ! toggleButton.getToggleState() ? Colours::dimgrey : Colour(0u, 172u, 1u);
    g.setColour(color);
    
    auto bounds = toggleButton.getLocalBounds();
    g.drawRect(bounds);
    
    g.strokePath(button->randomPath, PathStrokeType(1.f));
}

void AnalyzerButton::resized()
{
    auto bounds = getLocalBounds();
    auto insetRect = bounds.reduced(4);
    
    auto insetY = insetRect.getY();
    auto insetX = insetRect.getX();
    auto insetHeight = insetRect.getHeight();
    auto insetRight = insetRect.getRight();
    
    juce::Random r;
    
    randomPath.startNewSubPath(insetX, insetY + insetHeight * r.nextFloat());
    
    for (auto x = insetX + 1; x < insetRight; x+=2)
    {
        randomPath.lineTo(x, insetY + insetHeight * r.nextFloat());
    }
}

//==================================================================================//
void RotarySliderWithLabels::paint(juce::Graphics &g)
{
    using namespace juce;
    
    auto startAng = degreesToRadians(180.f + 45.f);
    auto endAng = degreesToRadians(180.f - 45.f) + MathConstants<float>::twoPi;
    
    auto range = getRange();
    
    auto sliderBounds = getSliderBounds();
    
    getLookAndFeel().drawRotarySlider(g, sliderBounds.getX(), sliderBounds.getY(), sliderBounds.getWidth(),
                                      sliderBounds.getHeight(),
                                      jmap(getValue(), range.getStart(), range.getEnd(), 0.0, 1.0),
                                      startAng, endAng, *this);
    
    auto center = sliderBounds.toFloat().getCentre();
    auto radius = sliderBounds.getWidth() * 0.5f;
    
    g.setColour(Colour(99u, 159u, 171u));
    g.setFont(getTextHeight());
    
    auto numChoices = labels.size();
    for (int i = 0; i < numChoices; i++)
    {
        Rectangle<float> r;
        
        auto pos = labels[i].pos;
        jassert(0.f <= pos);
        jassert(pos <= 1.f);
        
        auto ang = jmap(pos, 0.f, 1.f, startAng, endAng);
        
        auto c = center.getPointOnCircumference(radius + getTextHeight() * 0.5f + 1, ang);
        
        auto str = labels[i].label;
        r.setSize(g.getCurrentFont().getStringWidth(str), getTextHeight());
        r.setCentre(c);
        r.setY(r.getY() + getTextHeight());
        
        g.drawFittedText(str, r.toNearestInt(), Justification::centred, 1);
    }
}

juce::Rectangle<int> RotarySliderWithLabels::getSliderBounds() const
{
    juce::Rectangle<int> r;
    
    auto bounds = getLocalBounds();
    
    auto size = juce::jmin(bounds.getWidth(), bounds.getHeight());
    size -= (getTextHeight() * 2);
    
    r.setSize(size, size);
    r.setCentre(bounds.getCentreX(), 0);
    r.setY(2);
    
    return r;
}

juce::String RotarySliderWithLabels::getDisplayString() const
{
    if (auto* choiceParam = dynamic_cast<juce::AudioParameterChoice*>(param))
    {
        return choiceParam->getCurrentChoiceName();
    }
    
    juce::String str;
    bool addK = false;
    
    if (auto* floatParam = dynamic_cast<juce::AudioParameterFloat*>(param))
    {
        float value = getValue();
        if ( value > 1000.f)
        {
            value /= 1000.f;
            addK = true;
        }
        
        str = juce::String(value, (addK ? 2 : 0));
        
    }
    else
    {
        jassertfalse; // Sanity check
    }
    
    if (suffix.isNotEmpty())
    {
        str << " ";
        if (addK)
        {
            str << "k";
        }
        
        str << suffix;
    }
    
    return str;
}

//==============================================================================
SimpleDistAudioProcessorEditor::SimpleDistAudioProcessorEditor(SimpleDistAudioProcessor &p): AudioProcessorEditor(&p),
    audioProcessor(p),
    inputSlider(*audioProcessor.apvts.getParameter("Input Gain"), "dB"),
    driveSlider(*audioProcessor.apvts.getParameter("Drive Gain"), "dB"),
    outputSlider(*audioProcessor.apvts.getParameter("Output Gain"), "dB"),
    mixSlider(*audioProcessor.apvts.getParameter("Mix"), "%"),

    preLPSlider(*audioProcessor.apvts.getParameter("Pre Low-Pass"), "Hz"),
    preHPSlider(*audioProcessor.apvts.getParameter("Pre High-Pass"), "Hz"),
    postLPSlider(*audioProcessor.apvts.getParameter("Post Low-Pass"), "Hz"),
    postHPSlider(*audioProcessor.apvts.getParameter("Post High-Pass"), "Hz"),

    emphFreqSlider(*audioProcessor.apvts.getParameter("Emph Freq"), "Hz"),
    emphGainSlider(*audioProcessor.apvts.getParameter("Emph Gain"), "dB"),

    inputAttachment(audioProcessor.apvts, "Input Gain", inputSlider),
    outputAttachment(audioProcessor.apvts, "Output Gain", outputSlider),
    driveAttachment(audioProcessor.apvts, "Drive Gain", driveSlider),
    mixAttachment(audioProcessor.apvts, "Mix", mixSlider),

    preLPAttachment(audioProcessor.apvts, "Pre Low-Pass", preLPSlider),
    preHPAttachment(audioProcessor.apvts, "Pre High-Pass", preHPSlider),
    postLPAttachment(audioProcessor.apvts, "Post Low-Pass", postLPSlider),
    postHPAttachment(audioProcessor.apvts, "Post High-Pass", postHPSlider),

    emphFreqAttachment(audioProcessor.apvts, "Emph Freq", emphFreqSlider),
    emphGainAttachment(audioProcessor.apvts, "Emph Gain", emphGainSlider),

    bypassButtonAttachment(audioProcessor.apvts, "Bypass", bypassButton)
{
    // Finish setting up selector
    auto& parameter = *audioProcessor.apvts.getParameter("WaveShaper Wave");
    waveChoice.addItemList (parameter.getAllValueStrings(), 1);
    waveSelector = std::make_unique<juce::AudioProcessorValueTreeState::ComboBoxAttachment> (audioProcessor.apvts, "WaveShaper Wave", waveChoice);

    // Add labels for sliders
    preLPSlider.labels.add({0.f, "10Hz"});
    preLPSlider.labels.add({1.f, "20kHz"});
    preHPSlider.labels.add({0.f, "10Hz"});
    preHPSlider.labels.add({1.f, "20kHz"});
    postLPSlider.labels.add({0.f, "10Hz"});
    postLPSlider.labels.add({1.f, "20kHz"});
    postHPSlider.labels.add({0.f, "10Hz"});
    postHPSlider.labels.add({1.f, "20kHz"});

    emphFreqSlider.labels.add({0.f, "10Hz"});
    emphFreqSlider.labels.add({1.f, "20kHz"});
    emphGainSlider.labels.add({0.f, "-20dB"});
    emphGainSlider.labels.add({1.f, "20dB"});
    
    inputSlider.labels.add({0.f, "0dB"});
    inputSlider.labels.add({1.f, "40dB"});
    outputSlider.labels.add({0.f, "-40dB"});
    outputSlider.labels.add({1.f, "40dB"});
    mixSlider.labels.add({0.f, "0%"});
    mixSlider.labels.add({1.f, "100%"});
    driveSlider.labels.add({0.f, "0dB"});
    driveSlider.labels.add({1.f, "40dB"});


    // Set look and feel for buttons
    bypassButton.setLookAndFeel(&lnf);
    
    for (auto* comp : getComps())
    {
        addAndMakeVisible(comp);
    }
    
    auto safePtr = juce::Component::SafePointer<SimpleDistAudioProcessorEditor>(this);
    bypassButton.onClick = [safePtr]()
    {
        if (auto* comp = safePtr.getComponent())
        {
            auto bypassed = comp->bypassButton.getToggleState();
            
            comp->inputSlider.setEnabled(!bypassed);
            comp->outputSlider.setEnabled(!bypassed);
            comp->driveSlider.setEnabled(!bypassed);
            comp->mixSlider.setEnabled(!bypassed);

            comp->preLPSlider.setEnabled(!bypassed);
            comp->preHPSlider.setEnabled(!bypassed);
            comp->postLPSlider.setEnabled(!bypassed);
            comp->postHPSlider.setEnabled(!bypassed);

            comp->emphFreqSlider.setEnabled(!bypassed);
            comp->emphGainSlider.setEnabled(!bypassed);
        }
    };
    
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize(800, 600);
}

SimpleDistAudioProcessorEditor::~SimpleDistAudioProcessorEditor()
{
    bypassButton.setLookAndFeel(nullptr);
}

//==============================================================================
void SimpleDistAudioProcessorEditor::paint(juce::Graphics &g)
{
    // (Our component is opaque, so we must completely fill the background with a
    // solid colour)
    using namespace juce;
    g.fillAll (Colours::darkgrey);
}

void SimpleDistAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    
    auto bounds = getLocalBounds();
    bounds.removeFromTop(5);
    
    auto bypassArea = bounds.removeFromTop(bounds.getHeight() * 0.1);
    auto driveArea = bounds.removeFromTop(bounds.getHeight() * 0.33);
    auto filterArea = bounds.removeFromTop(bounds.getHeight() * 0.66);
    auto emphArea = bounds.removeFromBottom(bounds.getHeight() * 0.66);
    auto waveArea = bounds.removeFromBottom(bounds.getHeight() * 0.33);

    bypassButton.setBounds(bypassArea.removeFromLeft(bypassArea.getWidth() * 0.5));
    mixSlider.setBounds(bypassArea.removeFromRight(bypassArea.getWidth()*0.5));

    inputSlider.setBounds(driveArea.removeFromLeft(driveArea.getWidth() * 0.3));
    outputSlider.setBounds(driveArea.removeFromLeft(driveArea.getWidth() * 0.6));
    driveSlider.setBounds(driveArea.removeFromLeft(driveArea.getWidth() * 0.9));

    preLPSlider.setBounds(filterArea.removeFromLeft(filterArea.getWidth() * 0.2));
    preHPSlider.setBounds(filterArea.removeFromLeft(filterArea.getWidth() * 0.4));
    postLPSlider.setBounds(filterArea.removeFromLeft(filterArea.getWidth() * 0.6));
    postHPSlider.setBounds(filterArea.removeFromLeft(filterArea.getWidth() * 0.8));

    emphFreqSlider.setBounds(emphArea.removeFromLeft(emphArea.getWidth() * 0.4));
    emphGainSlider.setBounds(emphArea.removeFromRight(emphArea.getWidth() * 0.8));

    waveChoice.setBounds(waveArea.removeFromRight(waveArea.getWidth() * 0.38));
}

std::vector<juce::Component *> SimpleDistAudioProcessorEditor::getComps()
{
    return {
      &inputSlider, &driveSlider, &outputSlider,
      &preLPSlider, &preHPSlider, &postLPSlider, &postHPSlider,
      &emphFreqSlider, &emphGainSlider, &mixSlider,
      &bypassButton, &waveChoice,
    };
}
