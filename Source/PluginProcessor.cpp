/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

template<typename Type>
Distortion<Type>::Distortion(){ }

template<typename Type>
void Distortion<Type>::updateWaveShaper()
{
    auto& waveshaper = processorChain.template get<WaveShaperIndex>();
    
    auto var = settings.algorithm;

    if (settings.algorithm == AlgorithmTanh)
    {
        waveshaper.functionToUse = [] (Type x)
        {
            return std::tanh(x);
        };
    }
    else if (settings.algorithm == AlgorithmAtan)
    {
        waveshaper.functionToUse = [] (Type x)
        {
            const auto factor = 2.f / juce::MathConstants<float>::pi;
            return (std::atan(x) * factor);
        };
    }
    else if (settings.algorithm == AlgorithmHardClip)
    {
        waveshaper.functionToUse = [] (Type x)
        {
            return juce::jlimit(-1.f, 1.f, x);
        };
    }
    else if (settings.algorithm == AlgorithmRectifier)
    {
        waveshaper.functionToUse = [] (Type x)
        {
            return std::abs(x);
        };
    }
    else if (settings.algorithm == AlgorithmSine)
    {
        waveshaper.functionToUse = [] (Type x)
        {
            return std::sin(x);
        };
    }
}

template<typename Type>
void Distortion<Type>::prepare(const juce::dsp::ProcessSpec& spec)
{
    // Update the values from the low pass filter
    updateFilters(spec.sampleRate);

    inputVolume.prepare(spec);

    mixBuffer.setSize(spec.numChannels, spec.maximumBlockSize);
    
    updateWaveShaper();
    processorChain.prepare(spec);

    outputVolume.prepare(spec);
}

template <typename Type>
void Distortion<Type>::process (juce::AudioBuffer<Type>& buffer, const float& sampleRate, const int& numChannels) noexcept
{
    // Bypass
    if (settings.bypassed)
    {
        return;
    }

    const auto numSamples = buffer.getNumSamples();

    // Processing
    juce::dsp::AudioBlock<float> block(buffer);
    juce::dsp::ProcessContextReplacing<float> context(block);

    // Update the values for the gains and the filters
    updateFilters(sampleRate);
    updateGain();
    updateWaveShaper();
    
    // Process Input Gain
    inputVolume.process(context);

    // Copy the buffer to dry buffer
    for (auto channel = 0; channel < numChannels; channel++)
        mixBuffer.copyFrom(channel, 0, buffer, channel, 0, numSamples);
    
    // Process the chain
    processorChain.process(context);

    // Process Mix and output gain
    for (auto channel = 0; channel < numChannels; channel++)
    {
        auto* dryChannelData = mixBuffer.getReadPointer(channel);
        auto* wetChannelData = buffer.getWritePointer(channel);

        for (int sample = 0; sample < buffer.getNumSamples(); ++sample)
        {
            wetChannelData[sample] = (dryChannelData[sample] * (1.0f - (settings.mix / 100.f)) + (wetChannelData[sample] * (settings.mix / 100.f)));
        }
    }

    // Process Output Gain
    auto var = outputVolume.getGainLinear();
    outputVolume.process(context);
}

template<typename Type>
void Distortion<Type>::reset() noexcept
{
    mixBuffer.applyGain(0.f);
    processorChain.reset();

    inputVolume.reset();
    outputVolume.reset();
}

template<typename Type>
void Distortion<Type>::updateGain()
{
    auto& driveGain = processorChain.template get<DriveGainIndex>();
    driveGain.setGainDecibels(settings.drive);

    inputVolume.setGainDecibels(settings.input);
    outputVolume.setGainDecibels(settings.output);
}

template<typename Type>
void Distortion<Type>::updateFilters(const float& sampleRate)
{
    auto pre_lp_coeff = FilterCoefs::makeFirstOrderLowPass (sampleRate, settings.pre_lp);
    *processorChain.template get<PreLPIndex>().state = *pre_lp_coeff;
    auto pre_hp_coeff = FilterCoefs::makeFirstOrderHighPass (sampleRate, settings.pre_hp);
    *processorChain.template get<PreHPIndex>().state = *pre_hp_coeff;

    auto post_lp_coeff = FilterCoefs::makeFirstOrderLowPass (sampleRate, settings.post_lp);
    *processorChain.template get<PostLPIndex>().state = *post_lp_coeff;
    auto post_hp_coeff = FilterCoefs::makeFirstOrderHighPass (sampleRate, settings.post_hp);
    *processorChain.template get<PostHPIndex>().state = *post_hp_coeff;

    auto pre_emp_coeff = FilterCoefs::makeLowShelf (sampleRate, settings.emp_freq, 1.f / sqrt(2.f), juce::Decibels::decibelsToGain(settings.emp_gain));
    *processorChain.template get<PreEMPIndex>().state = *pre_emp_coeff;
    auto post_emp_coeff = FilterCoefs::makeLowShelf (sampleRate, settings.emp_freq, 1.f / sqrt(2.f), juce::Decibels::decibelsToGain(-settings.emp_gain));
    *processorChain.template get<PostEMPIndex>().state = *pre_emp_coeff;
}

template<typename Type>
void Distortion<Type>::updateChainSettings(const ChainSettings& chainSettings)
{
     settings = chainSettings;
}

//==============================================================================
SimpleDistAudioProcessor::SimpleDistAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
}

SimpleDistAudioProcessor::~SimpleDistAudioProcessor()
{
}

//==============================================================================
const juce::String SimpleDistAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool SimpleDistAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool SimpleDistAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool SimpleDistAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double SimpleDistAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int SimpleDistAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int SimpleDistAudioProcessor::getCurrentProgram()
{
    return 0;
}

void SimpleDistAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String SimpleDistAudioProcessor::getProgramName (int index)
{
    return {};
}

void SimpleDistAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void SimpleDistAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    juce::dsp::ProcessSpec spec;
    
    spec.sampleRate = sampleRate;
    spec.maximumBlockSize = samplesPerBlock;
    spec.numChannels = 2;
    
    // prepare distortion
    distortion.prepare(spec);
}

void SimpleDistAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool SimpleDistAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void SimpleDistAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    const auto numChannels = juce::jmin(totalNumInputChannels, totalNumOutputChannels);
    const auto numSamples = buffer.getNumSamples();

    // Clear the buffer
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // Process the distortion
    distortion.updateChainSettings(updateChainSettings(apvts));
    distortion.process(buffer, getSampleRate(), numChannels);

    // Hard clipper limiter
    for (auto channel = 0; channel < numChannels; channel++)
    {
        auto* channelData = buffer.getWritePointer(channel);

        for (auto i = 0; i < numSamples; i++)
        {
            channelData[i] = juce::jlimit(-2.f, 2.f, channelData[i]);
        }
    }
}

//==============================================================================
bool SimpleDistAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* SimpleDistAudioProcessor::createEditor()
{
    return new SimpleDistAudioProcessorEditor (*this);
}

//==============================================================================
void SimpleDistAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
    
    juce::MemoryOutputStream mos(destData, true);
    apvts.state.writeToStream(mos);
}

void SimpleDistAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    
    auto tree = juce::ValueTree::readFromData(data, sizeInBytes);
    if (tree.isValid())
    {
        apvts.replaceState(tree);
    }
}

juce::AudioProcessorValueTreeState::ParameterLayout SimpleDistAudioProcessor::createParameterLayout()
{
    juce::AudioProcessorValueTreeState::ParameterLayout layout;
    
    // Set up the ranges for the parameters
    auto inputRange = juce::NormalisableRange<float>(0.f, 40.f);
    auto outputRange = juce::NormalisableRange<float>(-40.f, 40.f);
    auto mixRange = juce::NormalisableRange<float>(0.f, 100.f);
    auto driveRange = juce::NormalisableRange<float>(0.f, 40.f);

    auto preLPRange = juce::NormalisableRange<float>(10.f, 20000.f, 0.f, 0.5f);
    auto preHPRange = juce::NormalisableRange<float>(10.f, 20000.f, 0.f, 0.5f);
    auto postLPRange = juce::NormalisableRange<float>(10.f, 20000.f, 0.f, 0.5f);
    auto postHPRange = juce::NormalisableRange<float>(10.f, 20000.f, 0.f, 0.5f);

    auto empFreqRange = juce::NormalisableRange<float>(10.f, 20000.f, 0.f, 0.5f);
    auto empGainRange = juce::NormalisableRange<float>(-20.f, 20.f);

    
    // Adding the parameters to the layout.
    layout.add(std::make_unique<juce::AudioParameterFloat>( juce::ParameterID{"Input Gain", versionHint}, "Input Gain", inputRange, 0.f));
    layout.add(std::make_unique<juce::AudioParameterFloat>( juce::ParameterID{"Output Gain", versionHint}, "Output Gain", outputRange, 10.f));
    layout.add(std::make_unique<juce::AudioParameterFloat>( juce::ParameterID{"Mix", versionHint}, "Mix", mixRange, 100.f));
    layout.add(std::make_unique<juce::AudioParameterFloat>( juce::ParameterID{"Drive Gain", versionHint}, "Drive Gain", driveRange, 20.f));


    layout.add(std::make_unique<juce::AudioParameterFloat>( juce::ParameterID{"Pre Low-Pass", versionHint}, "Pre Low-Pass", preLPRange, 20000.f));
    layout.add(std::make_unique<juce::AudioParameterFloat>( juce::ParameterID{"Pre High-Pass", versionHint}, "Pre High-Pass", preHPRange, 20.f));
    layout.add(std::make_unique<juce::AudioParameterFloat>( juce::ParameterID{"Post Low-Pass", versionHint}, "Post Low-Pass", postLPRange, 20000.f));
    layout.add(std::make_unique<juce::AudioParameterFloat>( juce::ParameterID{"Post High-Pass", versionHint}, "Post High-Pass", postHPRange, 20.f));

    layout.add(std::make_unique<juce::AudioParameterFloat>( juce::ParameterID{"Emph Freq", versionHint}, "Emph Freq", empFreqRange, 700.f));
    layout.add(std::make_unique<juce::AudioParameterFloat>( juce::ParameterID{"Emph Gain", versionHint}, "Emph Gain", empGainRange, 0.f));

    // Add Waveshaper wave
    layout.add(std::make_unique<juce::AudioParameterChoice>( juce::ParameterID{"WaveShaper Wave", versionHint}, "WaveShaper Wave", ChainSettings().algorithms, 1));

    // Add Bypass button
    layout.add(std::make_unique<juce::AudioParameterBool>( juce::ParameterID{"Bypass", versionHint}, "Bypass", false));
    
    return layout;
}


ChainSettings SimpleDistAudioProcessor::updateChainSettings(juce::AudioProcessorValueTreeState& apvts)
{
    ChainSettings settings;
    
    settings.input = apvts.getRawParameterValue("Input Gain")->load();
    settings.drive = apvts.getRawParameterValue("Drive Gain")->load();
    settings.mix = apvts.getRawParameterValue("Mix")->load();
    settings.output = apvts.getRawParameterValue("Output Gain")->load();

    settings.pre_lp = apvts.getRawParameterValue("Pre Low-Pass")->load();
    settings.pre_hp = apvts.getRawParameterValue("Pre High-Pass")->load();
    settings.post_lp = apvts.getRawParameterValue("Post Low-Pass")->load();
    settings.post_hp = apvts.getRawParameterValue("Post High-Pass")->load();

    settings.emp_freq = apvts.getRawParameterValue("Emph Freq")->load();
    settings.emp_gain = apvts.getRawParameterValue("Emph Gain")->load();

    settings.algorithm = apvts.getRawParameterValue("WaveShaper Wave")->load();
    settings.bypassed = apvts.getRawParameterValue("Bypass")->load();
    
    return settings;
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new SimpleDistAudioProcessor();
}
