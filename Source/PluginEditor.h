/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "PluginProcessor.h"
#include <JuceHeader.h>



// Util functions
inline juce::String addKiloPrefix(float value);



//==================================================================================//
// Interface for custom toggle buttons
struct CustomToggleButton : juce::ToggleButton
{
    virtual void drawToggleButton(juce::Graphics &g,
                                  CustomToggleButton &toggleButton,
                                  bool shouldDrawButtonAsHighlighted,
                                  bool shouldDrawButtonDown) = 0;
};

struct PowerButton : CustomToggleButton
{
    void drawToggleButton(juce::Graphics &g,
                          CustomToggleButton &toggleButton,
                          bool shouldDrawButtonAsHighlighted,
                          bool shouldDrawButtonDown) override;
    bool hitTest (int x, int y) override;
};

struct AnalyzerButton : CustomToggleButton
{
    void drawToggleButton(juce::Graphics &g,
                          CustomToggleButton &toggleButton,
                          bool shouldDrawButtonAsHighlighted,
                          bool shouldDrawButtonDown) override;
    void resized() override;
    
    juce::Path randomPath;
};

//==============================================================================

struct MyLookAndFeel : juce::LookAndFeel_V4
{
        void drawRotarySlider(juce::Graphics&,
                              int x, int y, int width, int height,
                              float sliderPosProportional,
                              float rotaryStartAngle,
                              float rotaryEndAngle,
                              juce::Slider&) override;
        void drawToggleButton (juce::Graphics &g,
                               juce::ToggleButton & toggleButton,
                               bool shouldDrawButtonAsHighlighted,
                               bool shouldDrawButtonAsDown) override;
};



//==============================================================================
struct RotarySliderWithLabels : juce::Slider
{
public:
    RotarySliderWithLabels(juce::RangedAudioParameter& rap, const juce::String& unitSuffix) :
    juce::Slider(juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag,
    juce::Slider::TextEntryBoxPosition::NoTextBox),
    param(&rap),
    suffix(unitSuffix)
    {
        setLookAndFeel(&lnf);
    }
    
    struct LabelPos
    {
        float pos;
        juce::String label;
    };
    
    juce::Array<LabelPos> labels;
    
    ~RotarySliderWithLabels()
    {
        setLookAndFeel(nullptr);
    }
    
    void paint(juce::Graphics& g) override;
    juce::Rectangle<int> getSliderBounds() const;
    int getTextHeight() const { return 14; }
    juce::String getDisplayString() const;
    
private:
    MyLookAndFeel lnf;
    
    juce::RangedAudioParameter* param;
    juce::String suffix;
};


//==============================================================================
/**
 */
class SimpleDistAudioProcessorEditor : public juce::AudioProcessorEditor {
public:
    SimpleDistAudioProcessorEditor(SimpleDistAudioProcessor &);
    ~SimpleDistAudioProcessorEditor() override;

    //==============================================================================
    void paint(juce::Graphics &) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    SimpleDistAudioProcessor &audioProcessor;
    
    //--Sliders--//
    RotarySliderWithLabels inputSlider, driveSlider, outputSlider;
    RotarySliderWithLabels preLPSlider, preHPSlider, postLPSlider, postHPSlider;
    RotarySliderWithLabels emphFreqSlider, emphGainSlider, mixSlider;
    
    //--Buttons--//
    PowerButton bypassButton;

    //--Selector--//
    juce::ComboBox waveChoice;
    using SelectorAttachment = std::unique_ptr<juce::AudioProcessorValueTreeState::ComboBoxAttachment>;
    SelectorAttachment waveSelector;
    
    //--Attachments--//
    using APVTS = juce::AudioProcessorValueTreeState;
    using Attachment = APVTS::SliderAttachment;
    
    Attachment inputAttachment, driveAttachment, outputAttachment;
    Attachment preLPAttachment, preHPAttachment, postLPAttachment, postHPAttachment;
    Attachment emphFreqAttachment, emphGainAttachment, mixAttachment;

    using ButtonAttachment = APVTS::ButtonAttachment;
    
    ButtonAttachment bypassButtonAttachment;
    
    //--Colours--//
    juce::Colour lightGrey = juce::Colour(34u, 34u, 34u);
    std::vector<juce::Component*> getComps();
    
    MyLookAndFeel lnf;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(SimpleDistAudioProcessorEditor)
};
