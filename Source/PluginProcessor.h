/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

/**
    Settings for the Processor chain, initialised with defaults
 */
struct ChainSettings
{
    juce::StringArray algorithms {"None", "Tanh", "ATan", "Hard Clipper", "Rectifier", "Sine"} ;

    int algorithm = 1;

    float input {0.f},
        drive {20.f},
        mix {100.f},
        output {10.f},

        pre_lp {2000.f},
        pre_hp {20.f},
        post_lp {2000.f},
        post_hp {20.f},

        emp_freq {700.f},
        emp_gain {0.f},

        bypassed {false};
};

/**
      
 */
template<typename Type>
class Distortion
{
public:
    //=============================================================================
    Distortion();

    void prepare (const juce::dsp::ProcessSpec& spec);
    void updateWaveShaper();

    void process (juce::AudioBuffer<Type>& buffer, const float& sampleRate, const int& numChannels) noexcept;
    void reset() noexcept;
    
    void updateChainSettings(const ChainSettings& chainSettings);
    
private:
    void updateGain();
    void updateFilters(const float& sampleRate);
    
    //==============================================================================
    ChainSettings settings;

    // Enum representing each thing in the chain.
    enum
    {
        PreLPIndex,
        PreHPIndex,
        PreEMPIndex,
        DriveGainIndex,
        WaveShaperIndex,
        PostLPIndex,
        PostHPIndex,
        PostEMPIndex,
    };

    // Enum representing the waveshapers
    enum
    {
        None = 0,
        AlgorithmTanh,
        AlgorithmAtan,
        AlgorithmHardClip,
        AlgorithmRectifier,
        AlgorithmSine,
    };
    
    using Filter = juce::dsp::ProcessorDuplicator<juce::dsp::IIR::Filter<Type>, juce::dsp::IIR::Coefficients<Type>>;
    using FilterCoefs = juce::dsp::IIR::Coefficients<Type>;

    juce::dsp::Gain<Type> inputVolume, outputVolume;

    // Chain pre_lp -> pre_hp -> pre_emp -> drive_gain -> waveshaper -> post_lp -> post_hp -> post_emp
    juce::dsp::ProcessorChain<Filter,
                              Filter,
                              Filter,
                              juce::dsp::Gain<Type>,
                              juce::dsp::WaveShaper<Type>,
                              Filter,
                              Filter,
                              Filter> processorChain;

    juce::AudioBuffer<float> mixBuffer;
};


//==============================================================================
/**
*/
class SimpleDistAudioProcessor  : public juce::AudioProcessor
                            #if JucePlugin_Enable_ARA
                             , public juce::AudioProcessorARAExtension
                            #endif
{
public:
    //==============================================================================
    SimpleDistAudioProcessor();
    ~SimpleDistAudioProcessor() override;

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    // For parameters
    static juce::AudioProcessorValueTreeState::ParameterLayout createParameterLayout();
    juce::AudioProcessorValueTreeState apvts {*this, nullptr, "Parameters", createParameterLayout()};
    
    static const int versionHint = 1;

private:
    Distortion<float> distortion;
    
    ChainSettings updateChainSettings(juce::AudioProcessorValueTreeState& apvts);

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SimpleDistAudioProcessor)
};
