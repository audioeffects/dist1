# simpledist

This project contains a simple distortion plugin using static waveshapers.

## Building (CMake)

```
git submodule update --init # To ensure JUCE is pulled and exists

cmake -B cmake-build 
cmake --build cmake-build
```

## Building (Projucer)

Open projucer and then build through the preferred method and system (Linux Makefiles or Mac)
